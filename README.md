byld mmxv
=========

byld mmxv consists of several components which need to be run separately,
see the "usage" section below for instructions.

install
=======

The following instructions assume you're running Ubuntu 14.04 LTS.

Currently "epeg" is used to generate thumbnails, install epeg somewhere in the
PATH:

    sudo apt-get install automake libtool
    git clone https://github.com/mattes/epeg
    ./autogen.sh
    make
    sudo make install
    sudo ldconfig

Also install jq, imagemagick and some python libraries:

    sudo apt-get install python3-pil python3-requests jq imagemagick



configuration
=============

Before you can use byld, you will need a configuration file, it should look
like this:

    {
        "byld": {
            "images directory": "/absolute/path/to/your/images",
            "images base url": "https://website/path/to/your/images/",
            "thumbnail sizes": {
                "cover": "400",
                "thumb": "x150",
                "view": "700"
            }
        }
    }

"images directory" is the folder on your filesystem which is the root of the
 folder hierarchy you want to serve with byld.

"images base url" is the same folder as seen by a web visitor, images should be
served using a web server like nginx or apache.

"thumbnail sizes" is used to specify how thumbnails should be generated.  byld expects
values for "cover", "thumb" and "view".  You only need to specify width or height,
 the other value will be calculated to keep a correct aspect ratio for the image.

To retain a nice grid layout using the old byld.php design use a max height value for
"thumb".  For example "x200" to specify a max height of 200 pixels.


usage
=====

byld consists of simple web application which serves thumbnails and image metadata,
and various tools which generate the data for the web application.

- bin/build

  This tool is used to generate all the thumbnails for your site.  Use this once
  when you start a new byld mmxv site, and run it again if images were added to
  the site when the server wasn't running.

- bin/rebuild

  Same as bin/build, except it regenerates all thumbnails.  Use this if you changed
  thumbnail sizes in byld.json.

- bin/configtest

  This tool can be used to test the configuration file  (byld.json) of your byld site

- bin/thumbnail

  The command used to generate the set of thumbnails for a single original image

- bin/metadata

  The command used to extract image metadata and construct a single index.json for
  a folder of images.

- bin/develop and bin/watch

  Used during development, there is no need to use these unless you're making
  changes to the byld mmxv source code


License
=======

Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.0.  See [LICENSE.txt](LICENSE.txt).
