/**
 *   This file is part of wald:find.
 *   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

require ('node-jsx').install ({extension: '.jsx'});

var ExifImage = require ('exif').ExifImage;
var fs = require ('fs');
var http = require ('http');
var path = require ('path');
var when = require ('when');
var index = require ('./index.jsx');

var root = '/home/kuno/www/marwei/omgeving/';

var getData = function (dir, filename) {
    var fullName = path.join (dir, filename);
    var stats = fs.statSync (fullName);
    var isDir = stats.isDirectory ();
    var data = { isDir: isDir, filename: filename, size: stats.size };

    if (isDir) {
        return when (data);
    }

    var deferred = when.defer ();

    try {
        new ExifImage({ image : fullName }, function (error, exifData) {
            if (error) {
                data.error = error.message;
            }  else {
                data.exif = exifData;
            }
            deferred.resolve (data);
        });
    } catch (error) {
        data.error = error.message;
        deferred.resolve (data);
    }

    return deferred.promise;
};

http.createServer (function (request, response) {
    console.log ('method', JSON.stringify (request.method));
    console.log ('url', JSON.stringify (request.url));
    console.log ('headers', JSON.stringify (request.headers));
    console.log ('statusCode', JSON.stringify (request.statusCode));

    var actualDir = path.join (root, request.url);
    if (actualDir.indexOf (root) !== 0) {
        return respond ('trying to sneak out of the web root??');
    }

    fs.readdir (actualDir, function (err, files) {
        response.writeHead(200, {'Content-Type': 'text/html'});
        if (err) {
            response.write ('<h1>' + err + '</h1>');
            response.end ();
        } else {
            when.map (files, function (file) {
                return getData (actualDir, file).then (function (fileData) {
                    return fileData;
                });
            }).then (function (data) {
                response.write (index (root, actualDir, data));
                response.end ();
            });
        }
    });
}).listen (3600);

fs.readFile(__dirname + '/../package.json', function (err, data) {
    if (err) {
        console.log('Error starting server');
    } else {
        var parsed = JSON.parse(data);
        console.log('byld mmxv version ' + parsed.version);
        console.log('Server started, listening on port 3600');
    }
});
