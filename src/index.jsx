/**
 *   This file is part of wald-elements
 *   Copyright (C) 2014  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'user strict';

/* eslint no-unused-vars: 0 */

var React = require('react');

var staticRoot = 'http://10.237.0.114/marwei/byld-xv/';

var metadata = function (data) {
    return {
        camera: data.image.Model,
        size: data.exif.ExifImageWidth
            + 'x' + data.exif.ExifImageHeight,
        date: data.exif.DateTimeOriginal
    };
};

var Dir = React.createClass({
    displayName: 'Dir',
    propTypes: {
        item: React.PropTypes.object.isRequired
    },
    render: function () {
        var fullPath = this.props.item.filename;
        var displayPath = this.props.item.filename.split('/').pop();

        return (
            <div>
                DIR: <a href={fullPath}>{displayPath}</a><br />
            </div>
        );
    }
});

var Image = React.createClass({
    displayName: 'Image',
    propTypes: {
        item: React.PropTypes.object.isRequired
    },
    render: function () {
        this.props.item.exif = metadata(this.props.item.exif);

        return (
            <div>
                IMAGE: {this.props.item.filename}<br />
                <pre>
                    {JSON.stringify(this.props.item, null, '    ')}
                </pre><br />
            </div>
        );
    }
});

var isThumbnail = function (filename) {
    var parts = filename.split('.');
    if (parts.length < 3) {
        return false;
    }

    var suffix = parts.pop();
    var thumbnail = parts.pop();
    var thumbnailNames = [ 'album', 'cover', 'thumb', 'view' ];
    return thumbnailNames.indexOf (thumbnail) !== -1;
};

var page = function (root, dir, data) {
    var back = null;
    if (root !== dir) {
        back = <Dir key=".." item={{isDir: true, filename: '..'}} />
    }

    var styleUrl = staticRoot + "byld.css";

    return React.renderToStaticMarkup(
        <html>
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="refresh" content="30" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />

                <title>React server-side rendering example</title>
                <link rel="stylesheet" href={styleUrl} type="text/css" />
            </head>
            <body>
                <p>root: {root}</p>
                <p>dir: {dir}</p>
                <p>data length: {data.length}</p>
                {back}
                <pre>
                    {data.map(function (item) {
                        if (item.isDir) {
                            return <Dir key={item.filename} item={item} />;
                        }

                        if (item.exif && !isThumbnail(item.filename)) {
                            return <Image key={item.filename} item={item} />;
                        }

                        return null;
                     })}
                </pre>
            </body>
        </html>
    );
}

module.exports = page;
