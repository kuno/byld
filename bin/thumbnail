#!/usr/bin/env python3
# -*- mode: python; -*-

#   This file is part of byld mmxv
#   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.

import datetime
import codecs
import json
import os
import pipes
import requests
import sys
from os.path import abspath, basename, curdir, dirname, join
from sre_compile import isstring


identify_ping = 'identify -format "%wx%h" -ping'

def log (msg):
    now = datetime.datetime.now().isoformat()
    print ("[byld %s] %s" % (now, msg))


def calc_other_dimension (newPrimary, primary, secondary):
    return int(float(secondary) / (float(primary) / float(newPrimary)))

def thumb(sizes, filename):
    if "." not in filename:
        print ("File has no suffix.  Skipping %s" % (filename))
        sys.exit(1)

    original = os.popen(identify_ping + ' ' + pipes.quote(filename)).read()
    (width, height) = original.strip().split("x")
    width = int(width)
    height = int(height)

    if width < 16 or height < 16:
        print ("Very small image size (%dx%d), something went wrong?  Skipping %s" %
               (width, height, filename))
        sys.exit(1)

    (base_filename, suffix) = filename.rsplit(".", 1)

    for (thumbnailType, size) in sizes.items():
        if size[0] == "x":
            new_height = int(size[1:])
            new_width = calc_other_dimension (new_height, height, width)
        else:
            new_width = int(size)
            new_height = calc_other_dimension (new_width, width, height)

        new_filename = ".".join ([ base_filename, thumbnailType, suffix ])
        if (os.path.isfile (new_filename)):
            continue

        cmd = ("epeg --width=%d --height=%d %s %s" % (
            new_width, new_height, pipes.quote(filename), pipes.quote(new_filename)))
        logCmd = ("epeg --width=%d --height=%d %s %s" % (
            new_width, new_height,
            pipes.quote(basename(filename)),
            pipes.quote(basename(new_filename))))
        log("Running " + logCmd)
        exit_code = os.system(cmd)
        if exit_code != 0:
            log("epeg failed on " + filename)
            try:
                # If epeg failed, clean up possibly broken files
                os.unlink(new_filename)
            except Exception as e:
                pass


def help ():
    print ("\n".join ([
        "byld mmxv",
        "copyright 2015  Kuno Woudt <kuno@frob.nl>",
        "",
        "This program is free software: you can redistribute it and/or modify",
        "it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.",
        "",
        "Usage:  bin/thumbnail <image.jpg>"
        ""
    ]))

if __name__ == '__main__':
    try:
        data = json.load (codecs.open ("byld.json", "rb", "utf-8"))
    except Exception as e:
        print ("ERROR: Failed to load byld.json: ", e)
        sys.exit (1)

    if len (sys.argv) != 2:
        help ()
        sys.exit (2)

    thumb(data["byld"]["thumbnail sizes"], abspath(sys.argv[1]))
