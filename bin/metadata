#!/usr/bin/env python3
# -*- mode: python; -*-

#   This file is part of byld mmxv
#   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.

import PIL
import PIL.ExifTags
import PIL.Image
import codecs
import configparser
import datetime
import json
import os
import pipes
import sys
from os.path import abspath, dirname, join, splitext

project_root = dirname (dirname (abspath (__file__)))
package_json = json.load (codecs.open (
    join (project_root, "package.json"), "rb", "utf-8"))
context = json.load (codecs.open (
    join (project_root, "context.json"), "rb", "utf-8"))


def log (msg):
    now = datetime.datetime.now ().isoformat ()
    print ("[byld %s] %s" % (now, msg))


def retrieve_metadata (filename):
    img = PIL.Image.open(filename)
    exif_data = img._getexif()
    if exif_data:
        exif = {
            PIL.ExifTags.TAGS[k]: v
            for k, v in exif_data.items()
            if k in PIL.ExifTags.TAGS
        }

        whitelist = [
            'DateTime',
            'DateTimeDigitized',
            'DateTimeOriginal',
            'Make',
            'Model',
            'Orientation',
        ]

        data = {
            'exif:' + k[0].lower() + k[1:]: v
            for k, v in exif.items()
            if k in whitelist
        }
    else:
        data = {}

    data['schema:fileSize'] = "%sB" % (os.path.getsize(filename))
    data['exif:width'], data['exif:height'] = img.size

    return data


def metadata (data, filename):
    (basename, suffix) = splitext (filename)
    inifile = basename + '.ini'
    jsonfile = basename + '.json'
    created_metadata = True

    info = {}
    info['dc:subject'] = {}
    ini = configparser.ConfigParser()
    if os.path.isfile (inifile):
        ini.read(inifile)
        try:
            info['dc:subject']['dc:description'] = ini.get('image', 'description').strip('"')
        except configparser.NoOptionError as e:
            pass
        except KeyError as e:
            pass

        try:
            info['dc:subject']['byld:rotate'] = int(ini.get('image', 'rotate').strip('"'))
        except configparser.NoOptionError as e:
            pass
        except KeyError as e:
            info['dc:subject']['byld:rotate'] = 0

    if os.path.isfile (jsonfile):
        try:
            info.update(json.load(codecs.open(jsonfile, "rb", "utf-8")))
            created_metadata = False
        except Exception as e:
            log ("WARNING: Failed to read existing metadata, %s" % (jsonfile))

    info['dc:subject'].update(retrieve_metadata (filename))

    info.update(context)
    info['dc:creator'] = {
        '@id': 'byld:version:' + package_json["version"],
        'dc:title': 'byld mmxv',
        'dc:hasVersion': package_json["version"]
    }

    base_url = basename.replace(data["images directory"], data["images base url"])
    if base_url == basename:
        log ("ERROR: Could not determine public URL for %s", filename)
        sys.exit (1)

    info['@id'] = base_url + '.json'
    info['dc:subject']['@id'] = base_url + suffix

    with codecs.open(jsonfile, "wb", "utf-8") as f:
        json.dump(info, f, sort_keys=True, indent=4)

    if created_metadata:
        log("Created metadata: " + pipes.quote (os.path.basename (jsonfile)))
    else:
        log("Updated metadata: " + pipes.quote (os.path.basename (jsonfile)))


def help ():
    print ("\n".join ([
        "byld mmxv",
        "copyright 2015  Kuno Woudt <kuno@frob.nl>",
        "",
        "This program is free software: you can redistribute it and/or modify",
        "it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.",
        "",
        "Usage:  bin/metadata <image.jpg>",
        ""
    ]))
    sys.exit (2)


if __name__ == '__main__':
    try:
        data = json.load (codecs.open ("byld.json", "rb", "utf-8"))
    except Exception as e:
        print ("ERROR: Failed to load byld.json: ", e)
        sys.exit (1)

    if len (sys.argv) != 2:
        help ()
        sys.exit (2)

    metadata(data['byld'], abspath(sys.argv[1]))
