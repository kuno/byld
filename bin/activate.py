#!/usr/bin/env python

#   This file is part of wald:meta
#   Copyright (C) 2014  Kuno Woudt <kuno@frob.nl>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys
from os.path import abspath, dirname, isdir, isfile, join

def uninstalled ():
    """ This will look for ../lib and include it in the sys.path if present,
    this is allows uninstalled applications to find their modules. """

    lib = join (dirname (dirname (abspath (__file__))), 'lib')
    if isdir (lib):
        sys.path[0] = lib


def virtualenv ():
    """ This will look for an application specific virtualenv in ../ve
    and activate it if present. """

    ve = join (dirname (dirname (abspath (__file__))), 've')
    if isdir (ve):
        ve_activate = join (ve, 'bin', 'activate_this.py')

    if isfile (ve_activate):
        exec (open (ve_activate).read (), dict (__file__=ve_activate))

